#ifndef INIT_H
#define INIT_H

#include "game.h"
#include "ennemy.h"

void init_map(struct GameState *gs, char *map, char *mask);

void init_player(struct GameState *gs);

struct GameState *init(struct GameState *gs, char *map, char *mask);

void quit(struct GameState *gs, SDL_Texture *texture);

#endif /* !INIT_H */
