#ifndef PIXEL_H
#define PIXEL_H

#include "game.h"

Uint8* pixelref(SDL_Surface *surf, int x, int y);

Uint32 getpixel(SDL_Surface *surface, int x, int y);

enum BLOCK get_block_at(struct GameState *gs, int x, int y);

#endif /* ! PIXEL_H */
