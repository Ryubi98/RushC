#ifndef MOVE_H
#define MOVE_H

#include "game.h"
#include "pixel.h"

int is_grounded(struct GameState *gs);

void move_up(struct GameState *gs, int move);

void move_down(struct GameState *gs, int move);

void move_left(struct GameState *gs, int move);

void move_right(struct GameState *gs, int move);

#endif /* ! MOVE_H */
