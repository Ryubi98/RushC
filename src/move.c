#include "move.h"

int is_grounded(struct GameState *gs)
{
    for (int i = 0; i < gs->player->surface->w; i++)
    {
        if (get_block_at(gs, gs->player->dst.x + i, gs->player->dst.y + 1 +
                    gs->player->surface->h) == WALL)
            return 1;
    }
    return 0;
}

static void check_death(struct GameState *gs, int dx, int dy)
{
    if (dx == 0)
    {
        if (dy == -1)//UP
        {
            for (int i = 0; i < gs->player->surface->w; i++)
            {
                if (get_block_at(gs, gs->player->dst.x + i, gs->player->dst.y)
                        == DAMAGE)
                {
                    gs->player->health -= 1;
                    return;
                }
                if (get_block_at(gs, gs->player->dst.x + i, gs->player->dst.y)
                        == FINISH)
                {
                    gs->player->finish = 1;
                    return;
                }
            }
        }
        else//DOWN
        {
            for (int i = 0; i < gs->player->surface->w; i++)
            {
                if (get_block_at(gs, gs->player->dst.x + i, gs->player->dst.y
                            + gs->player->surface->h) == DAMAGE)
                {
                    gs->player->health -= 1;
                    return;
                }
                if (get_block_at(gs, gs->player->dst.x + i, gs->player->dst.y +
                            gs->player->surface->h) == FINISH)
                {
                    gs->player->finish= 1;
                    return;
                }
            }
        }
    }
    else
    {
        if (dy == -1)//LEFT
        {
            for (int i = 0; i < gs->player->surface->h; i++)
            {
                if (get_block_at(gs, gs->player->dst.x, gs->player->dst.y + i)
                        == DAMAGE)
                {
                    gs->player->health -= 1;
                    return;
                }
                if (get_block_at(gs, gs->player->dst.x, gs->player->dst.y + i)
                        == FINISH)
                {
                    gs->player->finish = 1;
                    return;
                }

            }
        }
        else//RIGHT
        {
            for (int i = 0; i < gs->player->surface->h; i++)
            {
                if (get_block_at(gs, gs->player->dst.x + gs->player->surface->w,
                            gs->player->dst.y + i) == DAMAGE)
                {
                    gs->player->health -= 1;
                    return;
                }
                if (get_block_at(gs, gs->player->dst.x + gs->player->surface->w,
                            gs->player->dst.y + i) == FINISH)
                {
                    gs->player->finish = 1;
                    return;
                }

            }
        }

    }
}

void move_up(struct GameState *gs, int move)
{
    for (int i = 1; i <= move; i++)
    {
        for (int j = 0; j < gs->player->surface->w; j++)
        {
            if (get_block_at(gs, gs->player->dst.x + j, gs->player->dst.y - i)
                    == WALL)
            {
                gs->player->vel.y = 0;
                return;
            }
        }
        gs->player->dst.y -= 1;
        check_death(gs, 0, -1);
    }
}

void move_down(struct GameState *gs, int move)
{
    for (int i = 1; i <= move; i++)
    {
        for (int j = 0; j < gs->player->surface->w; j++)
        {
            if (get_block_at(gs, gs->player->dst.x + j, gs->player->dst.y + i +
                        gs->player->surface->h) == WALL)
                return;
        }
        gs->player->dst.y += 1;
        check_death(gs, 0, 1);
    }
}

void move_left(struct GameState *gs, int move)
{
    for (int i = 1; i <= move; i++)
    {
        for (int j = 0; j < gs->player->surface->h; j++)
        {
            if (get_block_at(gs, gs->player->dst.x - i, gs->player->dst.y + j)
                    == WALL)
                return;
        }
        gs->player->dst.x -= 1;
        check_death(gs, -1, 0);
    }
    gs->player->direction = 1;
    gs->player->surface = gs->player->surfaces_mirror[gs->player->random < 16];
}

void move_right(struct GameState *gs, int move)
{
    for (int i = 1; i <= move; i++)
    {
        for (int j = 0; j < gs->player->surface->h; j++)
        {
            if (get_block_at(gs, gs->player->dst.x + i + gs->player->surface->w,
                        gs->player->dst.y + j) == WALL)
                return;
        }
        gs->player->dst.x += 1;
        check_death(gs, 1, 0);
    }
    gs->player->direction = 0;
    gs->player->surface = gs->player->surfaces[gs->player->random < 16];
}
