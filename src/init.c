#include "init.h"

void init_map(struct GameState *gs, char *map_name, char *map_mask)
{
    gs->map = calloc(1, sizeof(struct Map));
    gs->map->surface = IMG_Load(map_name);
    gs->map->mask = IMG_Load(map_mask);

    gs->map->src.x = 0;
    gs->map->src.y = 0;
    gs->map->src.w = gs->map->surface->w;
    gs->map->src.h = gs->map->surface->h;

    gs->map->dst.x = 0;
    gs->map->dst.y = 0;
    gs->map->dst.w = gs->map->surface->w;
    gs->map->dst.h = gs->map->surface->h;
}

void init_player(struct GameState *gs)
{
    gs->player = calloc(1, sizeof(struct Player));

    gs->player->surfaces[0] = IMG_Load("img/Luigi_walk.png");
    gs->player->surfaces[1] = IMG_Load("img/Luigi_run.png");

    gs->player->surfaces_mirror[0] = IMG_Load("img/Luigi_walk_mirror.png");
    gs->player->surfaces_mirror[1] = IMG_Load("img/Luigi_run_mirror.png");

    gs->player->surface_static = IMG_Load("img/Luigi_static.png");
    gs->player->surface_static_mirror = IMG_Load("img/Luigi_static_mirror.png");

    gs->player->surface = gs->player->surface_static_mirror;

    gs->player->random = 0;
    gs->player->direction = 1;
    gs->player->health = 1;
    gs->player->finish = 0;

    gs->player->src.x = 0;
    gs->player->src.y = 0;
    gs->player->src.w = gs->player->surface->w;
    gs->player->src.h = gs->player->surface->h;

    gs->player->dst.x = 0;
    gs->player->dst.y = 0;
    gs->player->dst.w = gs->player->surface->w;
    gs->player->dst.h = gs->player->surface->h;
}

struct GameState *init(struct GameState *gs, char *map_name,
        char *map_mask)
{
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0)
    {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return NULL;
    }
    IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);
    Mix_Init(SDL_INIT_AUDIO | MIX_INIT_OGG | MIX_INIT_MP3);
    Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024);

    init_map(gs, map_name, map_mask);
    init_player(gs);
    init_ennemies(gs);

    gs->window = SDL_CreateWindow("Super Luigi Bros.",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            gs->map->surface->w, gs->map->surface->h, 0);
    gs->renderer = SDL_CreateRenderer(gs->window, -1, 0);

    return gs;
}

void quit(struct GameState *gs, SDL_Texture *texture)
{
    SDL_DestroyTexture(texture);

    SDL_FreeSurface(gs->map->surface);
    SDL_FreeSurface(gs->map->mask);

    free(gs->map);

    SDL_FreeSurface(gs->player->surfaces[0]);
    SDL_FreeSurface(gs->player->surfaces[1]);
    SDL_FreeSurface(gs->player->surfaces_mirror[0]);
    SDL_FreeSurface(gs->player->surfaces_mirror[1]);
    SDL_FreeSurface(gs->player->surface_static);
    SDL_FreeSurface(gs->player->surface_static_mirror);
    free(gs->player);

    for (int i = 0; i < gs->nb_ennemies; i++)
    {
        SDL_FreeSurface(gs->ennemies[i].surfaces[0]);
        SDL_FreeSurface(gs->ennemies[i].surfaces[1]);
        SDL_FreeSurface(gs->ennemies[i].surfaces_mirror[0]);
        SDL_FreeSurface(gs->ennemies[i].surfaces_mirror[1]);
        SDL_FreeSurface(gs->ennemies[i].surface_static);
        SDL_FreeSurface(gs->ennemies[i].surface_static_mirror);
    }
    free(gs->ennemies);

    SDL_DestroyRenderer(gs->renderer);
    SDL_DestroyWindow(gs->window);

    free(gs);
    Mix_CloseAudio();
    Mix_Quit();
    IMG_Quit();
    SDL_Quit();
}
