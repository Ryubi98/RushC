#include "game.h"
#include "init.h"
#include "move.h"

static void update(struct GameState *gs, int action[])
{
    float s = 2.0F;
    gs->player->acc.x = action[4] * s - action[3] * s;
    if (!is_grounded(gs))
    {
        gs->player->acc.y += 0.8F;
    }
    else
    {
        gs->player->acc.y = 0.0F;
        gs->player->vel.y = 0.0F;
    }
    if (action[UP])
    {
        gs->player->vel.y = 0.0F;
        gs->player->acc.y = -8.0F;
    }

    if (abs(gs->player->vel.x) < s)
        gs->player->vel.x += gs->player->acc.x * 0.16F;
    if (gs->player->vel.y < 5.0F)
        gs->player->vel.y += gs->player->acc.y * 0.16F;
    int pos_x = gs->player->vel.x;
    int pos_y = gs->player->vel.y;
    if (pos_x > 0)
        move_right(gs, pos_x);
    if (pos_x < 0)
        move_left(gs, -pos_x);
    if (pos_y >= 0)
        move_down(gs, pos_y);
    if (pos_y < 0)
        move_up(gs, -pos_y);
    move_ennemies(gs);
}

static void refresh(struct GameState *gs, SDL_Texture *texture_map)
{
    SDL_RenderCopy(gs->renderer, texture_map, &(gs->map->src), &(gs->map->dst));

    for (int i = 0; i < gs->nb_ennemies; i++)
    {
        SDL_Texture *texture = SDL_CreateTextureFromSurface(gs->renderer,
                gs->ennemies[i].surface);
        SDL_RenderCopy(gs->renderer, texture, &(gs->ennemies[i].src),
                &(gs->ennemies[i].dst));
    }

    SDL_Texture *texture = SDL_CreateTextureFromSurface(gs->renderer,
            gs->player->surface);
    SDL_RenderCopy(gs->renderer, texture, &(gs->player->src),
            &(gs->player->dst));

    SDL_RenderPresent(gs->renderer);
    SDL_Delay(16);
}

static int finish_scene(struct GameState *gs)
{
    SDL_Surface *surface = IMG_Load("img/win_scene.png");
    
    SDL_Rect src;
    SDL_Rect dst;
    src.x = 0;
    src.y = 0;
    src.w = surface->w;
    src.h = surface->h;

    dst.x = 0;
    dst.y = 0;
    dst.w = gs->map->surface->w;
    dst.h = gs->map->surface->h;

    SDL_Texture *texture = SDL_CreateTextureFromSurface(gs->renderer, surface);
    SDL_RenderCopy(gs->renderer, texture, &(src), &(dst));
    SDL_RenderPresent(gs->renderer);

    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
    
    while (1)
    {
        SDL_PumpEvents();
        int x;
        int y;
        int mask = SDL_GetMouseState(&x, &y);
        if (mask & SDL_BUTTON(SDL_BUTTON_LEFT))
        {
            if (x >= 511 && x <= 750 && y >= 641 && y <= 682)
                return 1;
            else if (x >= 517 && x <= 753 && y >= 727 && y <= 766)
                return 0;
        }
    }

    return 0;
}

static int gameover_scene(struct GameState *gs)
{
    SDL_Surface *surface = IMG_Load("img/gameover_scene.jpg");
    SDL_Rect src;
    SDL_Rect dst;
    src.x = 0;
    src.y = 0;
    src.w = surface->w;
    src.h = surface->h;

    dst.x = 0;
    dst.y = 0;
    dst.w = gs->map->surface->w;
    dst.h = gs->map->surface->h;

    SDL_Texture *texture = SDL_CreateTextureFromSurface(gs->renderer, surface);
    SDL_RenderCopy(gs->renderer, texture, &(src), &(dst));
    SDL_RenderPresent(gs->renderer);

    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
    while (1)
    {
        SDL_PumpEvents();
        int x;
        int y;
        int mask = SDL_GetMouseState(&x, &y);
        if (mask & SDL_BUTTON(SDL_BUTTON_LEFT))
        {
            if (x >= 25 && x <= 138 && y >= 922 && y <= 972)
                return 1;
            else if (x >= 1138 && x <= 1254 && y >= 922 && y <= 996)
                return 0;
        }
    }
    return 0;
}

static int title_screen()
{
    SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO);
    IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);

    SDL_Window *window = SDL_CreateWindow("Super Luigi Bros.",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            1280, 1024, 0);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);
    
    SDL_Surface *surface = IMG_Load("img/main_scene.png");
    
    SDL_Rect src;
    SDL_Rect dst;
    src.x = 0;
    src.y = 0;
    src.w = surface->w;
    src.h = surface->h;

    dst.x = 0;
    dst.y = 0;
    dst.w = 1280;
    dst.h = 1024;

    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_RenderCopy(renderer, texture, &(src), &(dst));
    SDL_RenderPresent(renderer);

    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
    
    int r = 2;
    while (r == 2)
    {
        SDL_PumpEvents();
        int x;
        int y;
        int mask = SDL_GetMouseState(&x, &y);
        if (mask & SDL_BUTTON(SDL_BUTTON_LEFT))
        {
            if (x >= 511 && x <= 750 && y >= 641 && y <= 682)
                r = 1;
            else if (x >= 517 && x <= 753 && y >= 727 && y <= 766)
                r = 0;
        }
    }


    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();
    return r;
}

int main(int argc, char *argv[])
{
    char *map_name;
    char *mask_name;
    if (argc == 3)
    {
        map_name = argv[1];
        mask_name = argv[2];
    }
    else
    {
        if (title_screen())
        {
            map_name = "img/map1.png";
            mask_name = "img/mask1.png";
        }
        else
            return 0;
    }

    int restart = 1;
    while (restart)
    {
        restart = 0;
        struct GameState *gs = calloc(1, sizeof(struct GameState));
        init(gs, map_name, mask_name);

        SDL_Texture *texture = SDL_CreateTextureFromSurface(gs->renderer,
                gs->map->surface);
        SDL_RenderCopy(gs->renderer, texture, &(gs->map->src), &(gs->map->dst));
        SDL_RenderPresent(gs->renderer);

        int action[5] = { 0 };
        while (1)
        {
            SDL_PumpEvents();
            const Uint8 *state = SDL_GetKeyboardState(NULL);
            if (state[SDL_SCANCODE_Q])
                break;
            if (state[SDL_SCANCODE_N])
            {
                restart = 1;
                map_name = "img/map2.png";
                mask_name = "img/mask2.png";
                break;
            }
            if ((action[3] && !state[SDL_SCANCODE_LEFT]) || (action[4] &&
                        !state[SDL_SCANCODE_RIGHT]))
                gs->player->vel.x = 0;
            action[1] = state[SDL_SCANCODE_UP];
            action[2] = state[SDL_SCANCODE_DOWN];
            action[3] = state[SDL_SCANCODE_LEFT];
            action[4] = state[SDL_SCANCODE_RIGHT];

            if (!is_grounded(gs))
                action[1] = 0;
            update(gs, action);
            gs->player->random = (gs->player->random + 1) % 32;
            for (int i = 0; i < gs->nb_ennemies; i++)
                gs->ennemies[i].random = (gs->ennemies[i].random + 1) % 32;
            if (gs->player->vel.x == 0.0F)
            {
                if (gs->player->direction)
                    gs->player->surface = gs->player->surface_static_mirror;
                else
                    gs->player->surface = gs->player->surface_static;
            }
            refresh(gs, texture);
            if (gs->player->health <= 0)
            {
                if (gameover_scene(gs))//restart
                    restart = 1;
                break;
            }
            if (gs->player->finish == 1)
            {
                if (finish_scene(gs))//next
                {
                    restart = 1;
                    map_name = "img/map2.png";
                    mask_name = "img/mask2.png";
                }
                break;
            }
        }
        quit(gs, texture);
    }
}
