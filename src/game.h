#ifndef GAME_H
#define GAME_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

enum ACTION
{
    DO_NOTHING,
    UP,
    DOWN,
    LEFT,
    RIGHT
};

enum BLOCK
{
    AIR,
    WALL,
    START,
    FINISH,
    DAMAGE,
    SPAWN
};

struct vector2
{
    float x;
    float y;
};

struct Player
{
    SDL_Surface *surface;
    SDL_Surface *surfaces[2];
    SDL_Surface *surfaces_mirror[2];
    SDL_Surface *surface_static;
    SDL_Surface *surface_static_mirror;
    int random;

    SDL_Rect src;
    SDL_Rect dst;

    struct vector2 vel;
    struct vector2 acc;

    int direction;
    int health;
    int finish;
};

struct Map
{
    SDL_Surface *surface;
    SDL_Surface *mask;
    SDL_Rect src;
    SDL_Rect dst;
};

struct GameState
{
    SDL_Window *window;
    SDL_Renderer *renderer;
    struct Map *map;
    struct Player *player;
    struct Ennemy *ennemies;
    int nb_ennemies;
};

#endif /* ! GAME_H */
