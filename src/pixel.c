#include "pixel.h"

Uint8* pixelref(SDL_Surface *surf, int x, int y)
{
    int bpp = surf->format->BytesPerPixel;
    return (Uint8*)surf->pixels + y * surf->pitch + x * bpp;
}

Uint32 getpixel(SDL_Surface *surface, int x, int y) 
{
    Uint8 *p = pixelref(surface, x, y);
    void *ptr = p;
    Uint16 *res16 = ptr;
    Uint32 *res32 = ptr;
    switch(surface->format->BytesPerPixel) 
    {
        case 1:
            return *p;
        case 2:
            return *res16;
        case 3:
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
                return p[0] << 16 | p[1] << 8 | p[2];
            else
                return p[0] | p[1] << 8 | p[2] << 16;
        case 4:
            return *res32;
    }
    return 0;
}

enum BLOCK get_block_at(struct GameState *gs, int i, int j)
{
    Uint32 pixel = getpixel(gs->map->mask, i, j);
    Uint8 r, b, g;
    SDL_GetRGB(pixel, gs->map->mask->format, &r, &g, &b);
    if (r == 0 && g == 0 && b == 0)
        return WALL;
    if (r == 0 && g == 0 && b == 255)
        return SPAWN;
    if (r == 255 && g == 0 && b == 0)
        return DAMAGE;
    if (r == 0 && g == 255 && b == 0)
        return FINISH;
    if (r == 255 && g == 255 && b == 0)
        return START;
    return AIR;
}
