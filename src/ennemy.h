#ifndef ENNEMY_H
#define ENNEMY_H

#include "game.h"
#include "pixel.h"

struct Ennemy
{
    SDL_Surface *surface;
    SDL_Surface *surfaces[2];
    SDL_Surface *surfaces_mirror[2];
    SDL_Surface *surface_static;
    SDL_Surface *surface_static_mirror;

    SDL_Rect src;
    SDL_Rect dst;

    struct vector2 vel;
    struct vector2 acc;    

    int random;
    int orientation;
};

void init_ennemies(struct GameState *gs);

void move_ennemies(struct GameState *gs);

#endif /* ! ENNEMY_H */
