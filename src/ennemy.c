#include "ennemy.h"

static struct vector2 *get_ennemies_position(struct GameState *gs)
{
    struct vector2 *v = calloc(1, sizeof(struct vector2));
    int nb_ennemies = 0;
    for (int i = 0; i < gs->map->mask->w; i++)
    {
        for (int j = 0; j < gs->map->mask->h; j++)
        {
            enum BLOCK blk = get_block_at(gs, i, j); 
            if (blk == SPAWN)
            {
                nb_ennemies += 1;
                v = realloc(v, sizeof(struct vector2) * (nb_ennemies + 1));
                struct vector2 coord =
                {
                    .x = i,
                    .y = j
                };
                v[nb_ennemies] = coord;
            }
            if (blk == START)
            {
                gs->player->dst.x = i;
                gs->player->dst.y = j;
            }
        }
    }
    v[0].x = nb_ennemies;
    return v;
}

static void make_round(struct GameState *gs)
{
    for (int i = 0; i < gs->nb_ennemies; i++)
    {
        int brk = 0;
        if (!gs->ennemies[i].orientation)
        {
            for (int j = 0; j < gs->ennemies[i].surface->h; j++)
            {
                if (get_block_at(gs, gs->ennemies[i].dst.x + 1 +
                            gs->ennemies[i].surface->w,
                            gs->ennemies[i].dst.y + j) == WALL)
                {
                    gs->ennemies[i].orientation = 1;
                    gs->ennemies[i].surface = gs->ennemies[i].surfaces_mirror[gs->ennemies[i].random < 16];
                    brk = 1;
                    break;
                }
            }
            if (brk)
                continue;
            for (int j = 0; j < gs->ennemies[i].surface->w; j++)
            {
                if (get_block_at(gs, gs->ennemies[i].dst.x + j + 1,
                            gs->ennemies[i].dst.y + 1 +
                            gs->ennemies[i].surface->h) != WALL)
                {
                    gs->ennemies[i].orientation = 1;
                    gs->ennemies[i].surface = gs->ennemies[i].surfaces_mirror[gs->ennemies[i].random < 16];
                    brk = 1;
                    break;
                }
            }
            if (brk)
                continue;
            gs->ennemies[i].dst.x += 1;
            gs->ennemies[i].surface = gs->ennemies[i].surfaces[gs->ennemies[i].random < 16];
        }
        else
        {
            for (int j = 0; j < gs->ennemies[i].surface->h; j++)
            {
                if (get_block_at(gs, gs->ennemies[i].dst.x - 1,
                            gs->ennemies[i].dst.y + j) == WALL)
                {
                    gs->ennemies[i].orientation = 0;
                    gs->ennemies[i].surface = gs->ennemies[i].surfaces[gs->ennemies[i].random < 16];
                    brk = 1;
                    break;
                }
            }
            if (brk)
                continue;
            for (int j = 0; j < gs->ennemies[i].surface->w; j++)
            {
                if (get_block_at(gs, gs->ennemies[i].dst.x + j - 1,
                            gs->ennemies[i].dst.y + 1 +
                            gs->ennemies[i].surface->h) != WALL)
                {
                    gs->ennemies[i].orientation = 0;
                    gs->ennemies[i].surface = gs->ennemies[i].surfaces[gs->ennemies[i].random < 16];
                    brk = 1;
                    break;
                }
            }
            if (brk)
                continue;
            gs->ennemies[i].dst.x -= 1;
            gs->ennemies[i].surface = gs->ennemies[i].surfaces_mirror[gs->ennemies[i].random < 16];
        }
    }
}

void init_ennemies(struct GameState *gs)
{
    struct vector2 *ennemies_position = get_ennemies_position(gs);
    int n = ennemies_position->x;
    gs->nb_ennemies = n;
    gs->ennemies = calloc(n, sizeof(struct Ennemy));
    for (int i = 0; i < n; i++)
    {
        gs->ennemies[i].surfaces[0] = IMG_Load("img/Mario_walk.png");
        gs->ennemies[i].surfaces[1] = IMG_Load("img/Mario_run.png");

        gs->ennemies[i].surfaces_mirror[0] =
            IMG_Load("img/Mario_walk_mirror.png");
        gs->ennemies[i].surfaces_mirror[1] =
            IMG_Load("img/Mario_run_mirror.png");

        gs->ennemies[i].surface_static = IMG_Load("img/Mario_static.png");
        gs->ennemies[i].surface_static_mirror =
            IMG_Load("img/Mario_static_mirror.png");

        gs->ennemies[i].surface = gs->ennemies[i].surface_static_mirror;
        gs->ennemies[i].random = 0;
        gs->ennemies[i].orientation = 1;

        gs->ennemies[i].src.x = 0;
        gs->ennemies[i].src.y = 0;
        gs->ennemies[i].src.w = gs->ennemies[i].surface->w;
        gs->ennemies[i].src.h = gs->ennemies[i].surface->h;

        gs->ennemies[i].dst.x = ennemies_position[i + 1].x;
        gs->ennemies[i].dst.y = ennemies_position[i + 1].y;
        gs->ennemies[i].dst.w = gs->ennemies[i].surface->w;
        gs->ennemies[i].dst.h = gs->ennemies[i].surface->h;

        int is_flying = 1;
        while (is_flying)
        {
            for (int j = 0; j < gs->ennemies[i].surface->w; j++)
            {
                if (get_block_at(gs, gs->ennemies[i].dst.x + j,
                            gs->ennemies[i].dst.y + 1 +
                            gs->ennemies[i].surface->h) == WALL)
                {
                    is_flying = 0;
                    break;
                }
            }
            gs->ennemies[i].dst.y += 1;
        }
    }
    free(ennemies_position);
}

void move_ennemies(struct GameState *gs)
{
    for (int i = 0; i < gs->nb_ennemies; i++)
    {
        float x = (gs->ennemies[i].dst.x + gs->ennemies[i].surface->w / 2) -
            (gs->player->dst.x + gs->player->surface->w / 2);
        float y = (gs->ennemies[i].dst.y + gs->ennemies[i].surface->h / 2) -
            (gs->player->dst.y + gs->player->surface->h / 2);
        float distance = sqrt((x * x) + (y * y));
        if (distance > 200.0F)
        {
            make_round(gs);
            continue;
        }
        else if (distance < 20.0F)
            gs->player->health -= 1;

        int brk = 0;
        if (gs->player->dst.x - gs->ennemies[i].dst.x > 0) //right
        {
            for (int j = 0; j < gs->ennemies[i].surface->h; j++)
            {
                if (get_block_at(gs, gs->ennemies[i].dst.x + 1 +
                            gs->ennemies[i].surface->w,
                            gs->ennemies[i].dst.y + j) == WALL)
                {
                    brk = 1;
                    break;
                }
            }
            if (brk)
                continue;
            for (int j = 0; j < gs->ennemies[i].surface->w; j++)
            {
                if (get_block_at(gs, gs->ennemies[i].dst.x + j + 1,
                            gs->ennemies[i].dst.y + 1 +
                            gs->ennemies[i].surface->h) != WALL)
                {
                    brk = 1;
                    break;
                }
            }
            if (brk)
                continue;
            gs->ennemies[i].dst.x += 1;
            gs->ennemies[i].orientation = 1;
            gs->ennemies[i].surface = gs->ennemies[i].surfaces[gs->ennemies[i].random < 16];
        }
        else if (gs->player->dst.x - gs->ennemies[i].dst.x < 0)
        {
            for (int j = 0; j < gs->ennemies[i].surface->h; j++)
            {
                if (get_block_at(gs, gs->ennemies[i].dst.x - 1,
                            gs->ennemies[i].dst.y + j) == WALL)
                {
                    brk = 1;
                    break;
                }
            }
            if (brk)
                continue;
            for (int j = 0; j < gs->ennemies[i].surface->w; j++)
            {
                if (get_block_at(gs, gs->ennemies[i].dst.x + j - 1,
                            gs->ennemies[i].dst.y + 1 +
                            gs->ennemies[i].surface->h) != WALL)
                {
                    brk = 1;
                    break;
                }
            }
            if (brk)
                continue;
            gs->ennemies[i].dst.x -= 1;
            gs->ennemies[i].orientation = 0;
            gs->ennemies[i].surface = gs->ennemies[i].surfaces_mirror[gs->ennemies[i].random < 16];
        }
        else
        {
            if (!gs->ennemies[i].orientation)
                gs->ennemies[i].surface = gs->ennemies[i].surface_static_mirror;
            else
                gs->ennemies[i].surface = gs->ennemies[i].surface_static;
        }
    }
}
