CC ?= gcc
CFLAGS = -Wall -Wextra -pedantic -std=c99 -lm
CFLAGS += $(shell sdl2-config --cflags)
LDLIBS += $(shell sdl2-config --libs) -lSDL2_image -lSDL2_mixer

BIN = game
SRC = $(wildcard src/*.c)

build: $(BIN)

$(BIN): $(SRC)
	$(CC) $(CFLAGS) $(LDLIBS) $(SRC) -o $(BIN)

run: $(BIN)
	./$(BIN)

clean:
	$(RM) $(BIN)

.PHONY: clean
